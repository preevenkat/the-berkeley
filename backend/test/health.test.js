const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const app = require('../app');
const should = chai.should();
const expect = chai.expect;

describe('GET /health', () => {
  it('should return health when called', done => {
    chai
      .request(app)
      .get('/health')
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.body).to.deep.equal({ status: "healthy" });
        done();
      });
  });
});