let express = require('express');
let router = express.Router();
const redis = require("redis");

const redisUrl = process.env.REDIS_URL || "http://localhost:6379"

const client = redis.createClient(redisUrl);
client.on("error", function(error) {
  console.error(error);
});

/* GET Counter. */
router.get('/counter', function (req, res, next) {
  client.incr("counter", function(err, counterValue) {
    console.log(counterValue.toString());
    res.send({ count: counterValue } );
  });
});

module.exports = router;