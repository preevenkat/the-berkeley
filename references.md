### The Berkeley Project

## Technologies



## References

* Adding an existing EKS cluster in Gitlab - https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster

* Adding additional users to the EKS cluster - https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html

# List Service Accounts
kubectl get sa -n kube-system

# Create ClusterRole, ClusterRoleBinding & ServiceAccount
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/master/docs/examples/rbac-role.yaml

# List Service Accounts
kubectl get sa -n kube-system

# Describe Service Account alb-ingress-controller 
kubectl describe sa alb-ingress-controller -n kube-system


# Create IAM Role for ALB ingress controller


# Replaced region, name, cluster and policy arn 
eksctl create iamserviceaccount \
    --region us-east-1 \
    --name alb-ingress-controller \
    --namespace kube-system \
    --cluster eksdemo1 \
    --attach-policy-arn <Policy_Name> \
    --override-existing-serviceaccounts \
    --approve
