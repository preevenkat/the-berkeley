import React, { useState, useEffect } from 'react';
import axios from 'axios';

import './App.css';

function App() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    axios.get('/api/counter')
      .then(function (response) {
        // handle success
        setCount(response.data.count)
        console.log(response);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  }, []);

  return (
    <div className="App">
      <h3>Welcome to the Berkeley Project</h3>
      <br />
     <div>This is the {count} visitor</div>
    </div>
  );
}

export default App;
